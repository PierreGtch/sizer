# Sizer

A little program to measure elements in an image

## Installation
This programm requires `python` and the libraries listed in `requirements.txt`.

## Running the programm
To executethe programm, please run:
```
python sizer.py [OPTIONS]
```

For more informations on the `[OPTIONS]`, please run:
```
python sizer.py -h
```
