from math import atan2, pi, copysign
from functools import partial
from collections import OrderedDict
import tkinter as tk
from tkinter import Frame, Entry, Button, Canvas, Label, Tk, filedialog, simpledialog, messagebox, font
from PIL import ImageTk, Image

CURRENT = 'current'

AUTOLINK_KEY = 'a'
MODE_KEY = 'x'
CLEAR_KEY = 'c'
SELECT_IMAGE_KEY = 's'
SET_LENGTH_KEY = 'l'


def closest_pos(xO, yO, x, y):
    xx = abs(x-xO)
    yy = abs(y-yO)
    angle = atan2(yy, xx)
    # snap_angles = [0, pi/4, pi/2]
    # thresholds = [pi/8, 3*pi/8]
    if angle<pi/8: # horizontal snap
        return x, yO
    if angle>3*pi/8: # vertical snap
        return xO, y
    # else 45 deg snap
    mid = (xx+yy)/2
    return copysign(mid, x-xO)+xO, copysign(mid, y-yO)+yO


class LastUpdatedOrderedDict(OrderedDict):
    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.move_to_end(key)
    def last(self):
        k = next(reversed(self))
        return (k, self[k])

def get_length(p0, p1):
    return ((p0.x-p1.x)**2 + (p0.y-p1.y)**2)**0.5

class Point:
    def __init__(self, x, y, point_id):
        self.x = x
        self.y = y
        self.point_id = point_id
        self.segments = []
    def add_segment(self, s):
        assert isinstance(s, Segment)
        self.segments.append(s)
    def remove_segment(self, s):
        self.segments.remove(s)

class Segment:
    def __init__(self, p0:Point, p1:Point, line_id:int, label_id:int, label_outline_id:int):
        self.p0 = p0
        self.p1 = p1
        self.line_id = line_id
        self.label_id = label_id
        self.label_outline_id = label_outline_id
    def get_points(self):
        return self.p0, self.p1

class ObjectContainer:
    def __init__(self):
        self.id_to_point = LastUpdatedOrderedDict()
        self.id_to_segment = LastUpdatedOrderedDict()
        # self.label_to_points = LastUpdatedOrderedDict()
        # self.label_to_segment = {}
        self.selected_items = []
        self.temp_segment = None
        self.temp_point = None
        self.last_point = None

    def is_selected(self, item_id:int):
        if not isinstance(item_id, int):
            raise ValueError
        return item_id in self.selected_items
    def set_unselected(self, item_id:int):
        if not isinstance(item_id, int):
            raise ValueError
        self.selected_items.remove(item_id)
    def set_selected(self, item_id:int):
        if not isinstance(item_id, int):
            raise ValueError
        if item_id not in self.selected_items:
            self.selected_items.append(item_id)
    def get_selected_list(self):
        return list(self.selected_items)

    def register_point(self, x, y, point_id):
        point = Point(x, y, point_id)
        self.id_to_point[point_id] = point
        return point
    def register_segment(self, p0, p1, line_id, label_id, label_outline_id):
        segment = Segment(p0, p1, line_id=line_id, label_id=label_id, label_outline_id=label_outline_id)
        p0.add_segment(segment)
        p1.add_segment(segment)
        self.id_to_segment[label_id] = segment
        return segment

    def unregister_segment(self, segment):
        p0, p1 = segment.get_points()
        p0.remove_segment(segment)
        p1.remove_segment(segment)
        if self.temp_segment==segment:
            self.temp_segment = None
        if self.is_selected(segment.label_id):
            self.set_unselected(segment.label_id)
            self.set_unselected(segment.line_id)
        del self.id_to_segment[segment.label_id]
        del segment
    def unregister_point(self, point):
        point_id = point.point_id
        if self.is_selected(point_id):
            self.set_unselected(point_id)
        if self.last_point==point:
            self.last_point = None
        if self.temp_point==point:
            self.temp_point = None
        del point

    def get_point(self, point_id, do_raise=True):
        point = self.id_to_point.get(point_id, None)
        if point is None and do_raise:
            raise ValueError(f'id not registered: {point_id}')
        return point
    def get_segment(self, segment_id, do_raise=True):
        segment = self.id_to_segment.get(segment_id, None)
        if segment is None and do_raise:
            raise ValueError(f'id not registered: {segment_id}')
        return segment

class App:
    def __init__(self, root, config):
        self.root = root
        self.config = config
        self.font = font.Font(size=config.font_size, family=config.font_family)

        self.top_menu = Frame(self.root)
        self.top_menu.pack()
        self.canvas = Canvas(self.root, width=config.canvas_width, height=config.canvas_height, bg='white')
        self.canvas.pack()

        self.btn_img = Button(self.top_menu, text=f'Select Image ({SELECT_IMAGE_KEY.upper()})', command=self.select_image, font=self.font)
        self.btn_img.grid(column=0, row=0)
        self.root.bind(f'<{SELECT_IMAGE_KEY}>', self.select_image)

        self.btn_clear = Button(self.top_menu, text=f'Clear ({CLEAR_KEY.upper()})', command=self.click_reset, font=self.font)
        self.btn_clear.grid(column=1, row=0)
        self.root.bind(f'<{CLEAR_KEY}>', self.click_reset)

        # self.btn_undo = Button(self.top_menu, text='Undo', command=self.undo, font=self.font)
        # self.btn_undo.grid(column=2, row=0)

        self.btn_undo = Button(self.top_menu, text=f'Set length ({SET_LENGTH_KEY.upper()})', command=self.click_set_length, font=self.font)
        self.btn_undo.grid(column=2, row=0)
        self.root.bind(f'<{SET_LENGTH_KEY}>', self.click_set_length)

        self.btn_mode = Button(self.top_menu, text='', command=self.change_mode, font=self.font)
        self.btn_mode.grid(column=3, row=0)
        self.root.bind(f'<{MODE_KEY}>', self.change_mode)

        self.btn_autolink = Button(self.top_menu, text='', command=self.change_autolink, font=self.font)
        self.btn_autolink.grid(column=4, row=0)
        self.root.bind(f'<{AUTOLINK_KEY}>', self.change_autolink)
        # self.btn_close = Button(self.top_menu, text='Close Polygon', command=self.close_polygon)
        # self.btn_close.grid(column=3, row=0)

        # self.label_status = Label(self.top_menu, text='', font=self.font)
        # self.label_status.grid(column=4, row=0)

        self.canvas.bind('<Button 1>', self.click_canvas)
        self.canvas.bind('<Shift Button 1>', partial(self.click_canvas, shift=True))
        self.canvas.bind('<B1-Motion>', partial(self.drag_canvas))
        self.canvas.bind('<Motion>',   self.move_canvas)
        self.canvas.bind('<Shift Motion>',   partial(self.move_canvas, shift=True))

        self.root.bind('<Escape>', self.click_unselect_all)
        self.root.bind('<Delete>', self.click_delete_selection)
        self.root.bind('<BackSpace>', self.click_delete_selection)

        self.reset()

    def change_mode(self, event=None):
        if self.construction:
            self._set_edit_mode()
        else:
            self._set_construction_mode()
    def _set_edit_mode(self):
        self.construction = False
        self.btn_mode.configure(text=f'Construction mode ({MODE_KEY.upper()})')
        self._unselect_all()
    def _set_construction_mode(self):
        self.construction = True
        self.btn_mode.configure(text=f'Edit mode ({MODE_KEY.upper()})')
        self._unselect_all()

    def _decorate_item(self, item_id):
        if self.objects.is_selected(item_id):
            self.canvas.itemconfigure(item_id, fill=self.config.selected_color)
            return
        item_type = self.canvas.type(item_id)
        if item_type=='oval':
            self.canvas.itemconfigure(item_id, fill=self.config.point_color, outline='white')
        elif item_type=='line':
            self.canvas.itemconfigure(item_id, fill=self.config.line_color)
        elif item_type=='text':
            self.canvas.itemconfigure(item_id, fill='black')
        else:
            raise ValueError(f'Unknown item type: {item_type}')
    def _set_last_point(self, p=None):
        if (self.objects.last_point is not None) and self.objects.is_selected(self.objects.last_point.point_id):
            self._unselect_item(self.objects.last_point.point_id)
        self.objects.last_point = p
        if p is not None:
            self._select_item(p.point_id)
    def _select_item(self, item_id):
        self.objects.set_selected(item_id)
        self._decorate_item(item_id)
    def _unselect_item(self, item_id):
        self.objects.set_unselected(item_id)
        self._decorate_item(item_id)
    def _click_select(self, item_id, item_type):
        if item_type=='text':
            segment = self.objects.get_segment(item_id)
            self._select_item(segment.line_id)
        self._select_item(item_id)
    def _click_unselect(self, item_id, item_type):
        if item_type=='text':
            segment = self.objects.get_segment(item_id)
            self._unselect_item(segment.line_id)
        self._unselect_item(item_id)
    def _unselect_all(self):
        while True:
            items = self.objects.get_selected_list()
            if len(items)==0:
                break
            item_id = items[-1]
            self._unselect_item(item_id)

    def click_unselect_all(self, event=None):
        if not self.construction:
            self._unselect_all()

    def _delete_segment(self, segment, delete_points=True):
        self.canvas.delete(segment.label_id)
        self.canvas.delete(segment.label_outline_id)
        self.canvas.delete(segment.line_id)
        p0, p1 = segment.get_points()
        self.objects.unregister_segment(segment)
        if delete_points:
            if len(p0.segments)==0:
                self._delete_point(p0)
            if len(p1.segments)==0:
                self._delete_point(p1)
    def _delete_point(self, point, delete_segments=False):
        if len(point.segments)!=0:
            if delete_segments:
                for segment in list(point.segments):
                    p, p2 = segment.get_points()
                    self._delete_segment(segment, delete_points=False)
                    if p==point:
                        p = p2
                    if len(p.segments)==0:
                        self._delete_point(p)
            else:
                raise ValueError('cant delete this point (segments connected)')
        self.canvas.delete(point.point_id)
        self.objects.unregister_point(point)

    def click_delete_selection(self, event=None):
        for item_id in self.objects.get_selected_list():
            item_type = self.canvas.type(item_id)
            if item_type=='text':
                segment = self.objects.get_segment(item_id)
                self._delete_segment(segment, delete_points=True)
            if item_type=='oval':
                point = self.objects.get_point(item_id)
                self._delete_point(point, delete_segments=True)

    def change_autolink(self, event=None):
        if not self.construction:
            return
        if self.autolink:
            self._unset_autolink()
        else:
            self._set_autolink()
    def _set_autolink(self):
        self.autolink = True
        self.btn_autolink.configure(text=f'Disable Autolink ({AUTOLINK_KEY.upper()})')
        if self.objects.last_point is None and len(self.objects.id_to_segment)>0:
            _,segment = self.objects.id_to_segment.last()
            _,p = segment.get_points()
            self._set_last_point(p)
    def _unset_autolink(self):
        self.autolink = False
        self.btn_autolink.configure(text=f'Enable Autolink ({AUTOLINK_KEY.upper()})')
        if self.objects.last_point is not None and len(self.objects.last_point.segments)!=0:
            self._set_last_point()

    def click_reset(self, event=None):
        answer = messagebox.askokcancel(title='Clear', message='Everithing will be deleted')
        if answer:
            self.reset()
    def reset(self, event=None):
        self.ratio = 1
        self.canvas.delete('all')
        self.objects = ObjectContainer()
        self._set_construction_mode()
        self._unset_autolink()
        self.move_sem_available = True

    def select_image(self, event=None):
        filename = filedialog.askopenfilename(filetypes=[('Images','*.jpg *.jpeg *.png')])
        if filename is None:
            return
        im = Image.open(filename)
        # resize image
        width, height = im.size
        if self.config.canvas_width/self.config.canvas_height > width/height:
            new_width = width/height*self.config.canvas_height
            new_height = self.config.canvas_height
        else:
            new_width = self.config.canvas_width
            new_height = height/width*self.config.canvas_width
        im = im.resize((int(new_width), int(new_height)))
        img = ImageTk.PhotoImage(im)
        self.canvas.create_image(self.config.canvas_width//2, self.config.canvas_height//2, image=img)
        self.canvas.image = img

    def _highlight(self, item_id):
        self.canvas.itemconfigure(item_id, fill=self.config.highlight_color)

    def _make_segment(self, p0, p1):
        line_id = self.canvas.create_line(0,0,0,0, tags=['line'], width=self.config.line_width)
        self.canvas.tag_raise('oval', line_id)
        label_outline_id = self.canvas.create_text(0, 0, text='', font=self.font, fill='white')
        label_id = self.canvas.create_text(0, 0, text='', font=self.font)
        segment = self.objects.register_segment(p0, p1, line_id=line_id, label_id=label_id, label_outline_id=label_outline_id)
        self._decorate_item(line_id)
        self._decorate_item(label_id)
        # self.label_to_segment[label_id] = line_id
        # self.label_to_points[label_id] = (p0,p1)
        self._update_segment_text(segment)
        self._update_segment_pos (segment)
        self.canvas.tag_bind(label_id, '<Enter>', partial(self.enter_label, segment))
        self.canvas.tag_bind(label_id, '<Leave>', partial(self.leave_label, segment))
        return segment
    def _make_point(self, x, y):
        r = self.config.point_radius
        point_id = self.canvas.create_oval(x-r, y-r, x+r, y+r, activefill=self.config.highlight_color)
        self.canvas.tag_raise('oval', point_id)
        self.canvas.addtag_withtag('oval', point_id)
        self._decorate_item(point_id)
        point_holder = self.objects.register_point(x, y, point_id)
        return point_holder
    def _move_point(self, point_id, x, y):
        point = self.objects.get_point(point_id)
        self.canvas.move(point_id, x-point.x, y-point.y)
        point.x = x
        point.y = y
        for segment in point.segments:
            self._update_segment_pos(segment)
            self._update_segment_text(segment)

    def enter_label(self, segment, event=None):
        if not self.construction:
            self._highlight(segment.label_id)
            self._highlight(segment.line_id)
    def leave_label(self, segment, event=None):
        if not self.construction:
            self._decorate_item(segment.label_id)
            self._decorate_item(segment.line_id)
    def _update_segment_text(self, segment):
        p0, p1 = segment.get_points()
        length = get_length(p0, p1) * self.ratio
        str_length = f'{length:.2f}'
        self.canvas.itemconfigure(segment.label_id, text=str_length)
        self.canvas.itemconfigure(segment.label_outline_id, text=str_length)
    def _update_segment_pos(self, segment):
        p0, p1 = segment.get_points()
        x = (p0.x+p1.x)//2
        y = (p0.y+p1.y)//2
        self.canvas.coords(segment.label_id, x, y)
        self.canvas.coords(segment.label_outline_id, x+self.config.outline_dx, y+self.config.outline_dy)
        self.canvas.coords(segment.line_id, p0.x, p0.y, p1.x, p1.y)

    def click_set_length(self, event=None):
        if self.construction:
            messagebox.showerror(title='Error', message='Please use the "Edit mode" to set a length <3')
            return
        selected_label = [i for i in self.objects.get_selected_list() if self.canvas.type(i)=='text']
        if len(selected_label)!=1:
            messagebox.showerror(title='Error', message='Please select exactly one line <3')
            return
        p0, p1 = self.objects.get_segment(selected_label[0]).get_points()
        self._set_length(p0, p1)
    def _set_length(self, p0, p1):
        length = simpledialog.askfloat('Set length', prompt='Set the new length of this segment :')
        if length is None:
                return
        old_length = get_length(p0, p1)
        self.ratio = length/old_length
        for segment in self.objects.id_to_segment.values():
            self._update_segment_text(segment)

    def _get_current(self):
        current = self.canvas.find_withtag(CURRENT)
        if len(current)==0:
            return None
        elif len(current)==1:
            return current[0]
        raise ValueError('multiple current')

    def click_canvas(self, event, shift=False):
        x, y = event.x, event.y
        current = self._get_current()
        current_type = None if current is None else self.canvas.type(current)
        if self.construction:
            if self.autolink or self.objects.last_point is None:
                if current_type=='oval' and (self.objects.temp_point is None or current!=self.objects.temp_point.point_id):
                    self._set_last_point(self.objects.get_point(current))
                else:
                    self._set_last_point(self.objects.temp_point)
            else:
                self._set_last_point()

            self.objects.temp_point = None
            self.objects.temp_segment = None

        else:
            if current is None:
                return
            if current_type in ['oval', 'text']:
                if shift and self.objects.is_selected(current):
                    self._click_unselect(current, current_type)
                else:
                    if not shift:
                        self._unselect_all()
                    self._click_select(current, current_type)

    def drag_canvas(self, event=None):
        x, y = event.x, event.y
        current = self._get_current()
        current_type = None if current is None else self.canvas.type(current)
        if not self.construction:
            if current_type=='oval':
                self._move_point(current, x=x, y=y)

    def _set_temp_view(self, x, y, show_segment, shift=False):
        current = self._get_current()
        current_type = None if current is None else self.canvas.type(current)

        ## select this_point:
        if current_type=='oval': # and (self.objects.temp_point is None or current!=self.objects.temp_point.point_id):
            this_point = self.objects.get_point(current)
        else:
            this_point = self._make_point(x, y)
            self.objects.temp_point = this_point
        if not show_segment:
            return
        ## select this_segment:
        this_segment = self._make_segment(self.objects.last_point, this_point)
        self.objects.temp_segment = this_segment


    def move_canvas(self, event=None, shift=False):
        if not self.move_sem_available:
            return
        else:
            self.move_sem_available = False
        x, y = event.x, event.y
        if self.objects.temp_segment is not None:
            self._delete_segment(self.objects.temp_segment, delete_points=(False))
        if self.objects.temp_point:
            self._delete_point(self.objects.temp_point)

        if self.construction:
            if self.objects.last_point is None:
                self._set_temp_view(x, y, show_segment=False)
            else:
                if shift:
                    x, y = closest_pos(self.objects.last_point.x, self.objects.last_point.y, x, y)
                self._set_temp_view(x, y, show_segment=True)
        else:
            pass
            # self._set_temp_view(self, x, y, show_segment=False)
        self.move_sem_available = True

    # def hover_canvas(self, event):
    #     current_hover = self.canvas.find_withtag(CURRENT)
    #     new_hover = [i for i in current_hover if i not in self.last_hover]
    #     expired_hover = [i for i in self.last_hover if i not in current_hover]
    #     # continued_hover = [i for i in self.last_hover if i in current_hover]
    #     self.last_hover = []
    #     print('current_hover', current_hover)
    #     print('new_hover', new_hover)
    #     for item_id in expired_hover:
    #         self._unhighlight(item_id)
    #         print('stop hover', item_id)
    #     for item_id in new_hover:
    #         if (self.construction and self.is_point(item_id)) or \
    #         (not self.construction and (self.is_point(item_id) or self.is_label(item_id))): # or self.is_segment(item_id))):
    #             self.last_hover.append(item_id)
    #             self._highlight(item_id)
    #             print('hover', item_id)

##########################################################
if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='<Escape>: to unselect all\n<Shift Click>: to select multiple lines\n<Delete>: to delete line(s)')
    parser.add_argument('-W', '--canvas_width', type=int, default=600, help='width of the canvas')
    parser.add_argument('-H', '--canvas_height', type=int, default=600, help='height of the canvas')
    parser.add_argument('-f', '--font_size', type=int, default=11, help='size of the characters')
    parser.add_argument('--font_family', type=str, default='dejavu sans', help='family font of the characters')
    parser.add_argument('-p', '--point_radius', type=int, default=2, help='radius of the points that will be drawn')
    parser.add_argument('--outline_dx', type=int, default=2)
    parser.add_argument('--outline_dy', type=int, default=2)
    parser.add_argument('-l', '--line_width', type=int, default=2, help='width of the lines that will be drawn')
    parser.add_argument('--line_color', type=str, default='grey', help='color of the lines that will be drawn')
    parser.add_argument('--point_color', type=str, default='black', help='color of the points that will be drawn')
    parser.add_argument('--highlight_color', type=str, default='blue', help='color of the highlighted elements')
    parser.add_argument('--selected_color', type=str, default='green', help='color of the selected elements')
    config = parser.parse_args()

    # create root window
    root = Tk()

    # check font
    if config.font_family not in font.families():
        print(f'font "{config.font_family}" unavailable.\nAvailable fonts:\n')
        for f in sorted(list(font.families())):
            print(f)

    # root window title and dimension
    root.title('Sizer')
    # Set geometry(widthxheight)
    root.geometry(f'{config.canvas_width+10}x{config.canvas_height+config.font_size*3}')
    App(root=root, config=config)

    # Execute Tkinter
    root.mainloop()
